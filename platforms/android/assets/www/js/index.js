/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var cordovaApp = {
    // Application Constructor
    initialize: function() {
        // device.platform
            window.localStorage.setItem('currentPlatform', 'ANDROID');
            // console.log(platform, 'ABABABAB1');
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        cordovaApp.receivedEvent('deviceready');
            console.log(device.platform, 'ABABABAB');
            var platform=device.platform
            window.localStorage.setItem('currentPlatform', platform);
            console.log(platform, 'ABABABAB1');

        push = PushNotification.init({
            "android": {
                "senderID": 981781408632,
                "forceShow":"true"
            },
            "ios": {
                "alert": 'true',
                "badge": true,
                "sound": 'false'
            },
            "windows": {}
        });


        push.on('registration', function(data) {
            console.log("Device registration: ", data); 
            const dataFormat= {
                "os":platform,
                "deviceId":data.registrationId
            }
            console.log(dataFormat, 'TESTTEST');
            console.log(JSON.stringify(dataFormat), 'TESTTEST!')
            window.localStorage.setItem('godysh-user-device', JSON.stringify(dataFormat))
            
            PushNotification.createChannel(
                function(){
                    console.log("Successfully able to create channel: godysh_consumer_push_test");
                },
                function(){
                    console.log("Failed to create channel: godysh_consumer_push_test");
                },
                {
                    id: "godysh_consumer_push_test",
                    description: "Godysh Consumer - Push Notification Test"
                }
            );
        });

        // const dataFormat= {
        //     "os":"IOS",
        //     "deviceId":deviceId
        // }
        // console.log(dataFormat, 'TESTTEST');
        // console.log(JSON.stringify(dataFormat), 'TESTTEST!')
        // window.localStorage.setItem('godysh-user-device', JSON.stringify(dataFormat));

        // push.on('notification', (data) => {
        //     console.log("message ->  ", data);
        // })
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // var parentElement = document.getElementById(id);
        // var listeningElement = parentElement.querySelector('.listening');
        // var receivedElement = parentElement.querySelector('.received');

        // listeningElement.setAttribute('style', 'display:none;');
        // receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
cordovaApp.initialize()