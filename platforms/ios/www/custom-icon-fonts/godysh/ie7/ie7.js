/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'godysh\'">' + entity + '</span>' + html;
	}
	var icons = {
		'gdy-show-more-button-with-three-dots': '&#xe908;',
		'gdy-add-plus-button': '&#xe909;',
		'gdy-substract': '&#xe90a;',
		'gdy-close-button': '&#xe90b;',
		'gdy-calendar': '&#xe90c;',
		'gdy-info-button': '&#xe90d;',
		'gdy-comment': '&#xe90e;',
		'gdy-hashtag': '&#xe90f;',
		'gdy-menu-button-of-three-lines': '&#xe910;',
		'gdy-dollar-symbol': '&#xe911;',
		'gdy-icon-4': '&#xe900;',
		'gdy-icon-5': '&#xe901;',
		'gdy-icon-6': '&#xe902;',
		'gdy-icon-7': '&#xe903;',
		'gdy-icon-8': '&#xe904;',
		'gdy-icon-1': '&#xe905;',
		'gdy-icon-2': '&#xe906;',
		'gdy-icon-3': '&#xe907;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/gdy-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
